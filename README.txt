-- SUMMARY --

This module creates a new permission for each content type:
  bypass requirements for X nodes

For a user with that permission, when editing an old node of that type, no
fields will be required.

The content type we use changes from time to time.  We often add new required
fields.  Meanwhile, our system administrators sometimes have to go and correct
old data.  When they do this, the data for the new required fields is not
available, but they can't save the form because the new fields are required.

With this module, a role can be given permission to ignore the requirements of
required fields.

-- REQUIREMENTS --

None, but supports CCK.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* You will need to set the weight of this module to be higher than the weight
  of CCK.  For example (mysql):
  UPDATE system SET weight=999 WHERE name='bypass_requirements';

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions »
  bypass_requirements module:

  - bypass requirements for X nodes

    Users in roles with this permission will have no required fields when
    editing nodes with the X content type.
    They will, however, have the normal required fields when creating a new
    node of that type.


-- TODO --

Even if you have permission to bypass requirements, this should not be the
default.  You should have to click some sort of button that turns on your
requirements-bypassing capabilities.


-- CONTACT --

Current maintainers:
* Ryan Hughes (galaxor) - http://drupal.org/user/154838/
